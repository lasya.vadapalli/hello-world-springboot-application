FROM openjdk:8-jdk-alpine
RUN addgroup -S lasyavadapalli && adduser -S lasyavadapalli -G lasyavadapalli
USER lasyavadapalli:lasyavadapalli
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]